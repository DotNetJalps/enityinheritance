﻿namespace EntityCodeFirstInheritance
{
    public class Employee : Person
    {
        public string Designation { get; set; }
    }
}